import {Injectable} from "@angular/core";

@Injectable()
export class SizeModel {

  private static sizes = {
    '1':['ONE'],
    '2':['32',34,36,38,40,42,44,46,48],
    '3':['XL','S','M','L','XS','XXL'],
    '4':['35',35.5,36,36.5,37,37.5,38,38.5,39,39.5,40,40.5,41],
    '5':['75',80,85,90,95,100]
  };
  private _size:any[];

  constructor(){}
  get size(): any {
    return this._size;
  }

   setSize(value: any) {
    switch (value){
      case 'Marketing Material':{
        this._size = SizeModel.sizes[1];
        break;
      }
      case 'Accessories':{
        this._size = SizeModel.sizes[4];
        break;
      }
      case 'Dresses':{
        this._size =SizeModel.sizes[2].concat(SizeModel.sizes[3]);
        break;
      }
      case 'Fives-pocket':{
        this._size = SizeModel.sizes[2];
        break;
      }
      case 'Gowns':{
        this._size = SizeModel.sizes[2];
        break;
      }
      case 'Jackets':{
        this._size = SizeModel.sizes[2].concat(SizeModel.sizes[3]);
        break;
      }
      case 'Knit jackets':{
        this._size = SizeModel.sizes[3];
        break;
      }
      case 'Leather':{
        this._size = SizeModel.sizes[2];
        break;
      }
      case 'Outerwear':{
        this._size = SizeModel.sizes[2];
        break;
      }
      case 'Pants':{
        this._size = SizeModel.sizes[2].concat(SizeModel.sizes[3]);
        break;
      }
      case 'Pullovers':{
        this._size = SizeModel.sizes[3];
        break;
      }
      case 'Skirts':{
        this._size = SizeModel.sizes[2].concat(SizeModel.sizes[3]);
        break;
      }
      case 'Top':{
        this._size = SizeModel.sizes[2].concat(SizeModel.sizes[3]);
        break;
      }
      case 'T-shirts':{
        this._size = SizeModel.sizes[3];
        break;
      }
      default :{
        this._size= SizeModel.sizes[2];
        break;
      }
    }

  }
}
