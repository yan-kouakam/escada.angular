export class ItemsModel {

  private _group:string;
  private _vendor:string;
  private _name:string;
  private _code_color:string;
  private _color:string;
  private _size:string;
  private _quantity:number;
  private _price:number;
  private _description:string;

  constructor(group: string, vendor: string,
              name: string, code_color: string,
              color: string, size: string,
              quantity: number, price: number, descritption: string) {
    this._group = group;
    this._vendor = vendor;
    this._name = name;
    this._code_color = code_color;
    this._color = color;
    this._size = size;
    this._quantity = quantity;
    this._price = price;
    this._description = descritption;
  }

  get group(): string {
    return this._group;
  }

  get vendor(): string {
    return this._vendor;
  }

  get name(): string {
    return this._name;
  }

  get code_color(): string {
    return this._code_color;
  }

  get color(): string {
    return this._color;
  }

  get size(): string {
    return this._size;
  }

  get quantity(): number {
    return this._quantity;
  }

  get price(): number {
    return this._price;
  }

  get description(): string {
    return this._description;
  }
}
