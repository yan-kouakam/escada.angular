import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { OrderComponent } from './order/order.component';
import { OrderListComponent } from './order/order-list/order-list.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HeaderComponent} from './header/header.component';
import {AppRoutingModule} from "./app.routing.module";
import { HomeComponent } from './home/home.component';
import {HttpModule} from "@angular/http";
import {OrderServices} from "../_services/order.services";
import { ClientsComponent } from './clients/clients.component';
import { SellsComponent } from './sells/sells.component';


@NgModule({
  declarations: [
    AppComponent,
    OrderComponent,
    OrderListComponent,
    HeaderComponent,
    HomeComponent,
    ClientsComponent,
    SellsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpModule,
  ],
  providers: [OrderServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
