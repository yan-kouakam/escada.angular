import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import 'rxjs/Rx';
import * as XLSX from 'xlsx';
import * as FileSaver from 'file-saver';
import {Subject} from "rxjs/Subject";

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable()
export class OrderServices{
  brandSelected = new Subject();
  seasonSelected = new Subject();

  constructor( private request:Http){}

  saveOrders(data:any){
    this.request.post('http://localhost:8000/api/v1/order',data)
      .map(
        (response:Response)=>{
          if (response.status!=404){
            return response.json();
          }
        }
      )
  }


  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'buffer' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  public exportFromExcelFile(fileName:string):any[]{
    //const worksheet: XLSX.WorkSheet =XLSX.utils.sheet_to_json();

    return [];
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + EXCEL_EXTENSION);
  }



}
